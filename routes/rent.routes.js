const { Router } = require('express')
const config = require('config')
const auth = require('../middlewares/auth.middleware')
//models
const Room = require('../models/Room.model')
const User = require('../models/User.model')

const router = Router()



router.param('roomID', async (req, res, next) => {
    try {
        const roomID = req.params.roomID

        const room = await Room.findById(roomID)

        if (!room) {
            res.status(404).json({ message: 'Room is not found' })
        }

        req.room = room

        next()
    } catch (e) {
        console.log(e)
    }
})


// /api/rent/:roomID
router.post('/:roomID', auth,
    async (req, res) => {
        try {

            if (req.room.guest) {
                return res.status(400).json({ message: 'Room is already busy' })
            }

            const userID = req.user.userID

            const user = await User.findById(userID)

            if (!user) {
                return res.status(404).json({ message: 'User is not found' })
            }

            const balance = user.balance
            const price = req.room.price

            if (balance >= price) {
                await User.findByIdAndUpdate(user._id, { $push: { rooms: req.room._id }, balance: balance - price })
                await Room.findByIdAndUpdate(req.room._id, { guest: user._id })
                return res.status(200).json({ message: 'ok' })
            } else {
                return res.status(400).json({ message: 'Less balance' })
            }

            res.status(200).json({ message: 'Room is rent' })
        } catch (e) {
            res.status(500).json({ message: `Smth has gone wrong. ${e.message}` })
        }
    }
)


// /api/rent/cancel/:roomID
router.post('/cancel/:roomID', auth,
    async (req, res) => {
        try {
            const { userID } = req.body
            console.log(userID, req.room.guest)
            if (userID == req.room.guest) {
                const room = await Room.findById(req.room._id)
                const user = req.user

                const balance = user.balance + room.price

                await Room.findByIdAndUpdate(req.room._id, { guest: undefined })
                await User.findByIdAndUpdate(userID, { $pull: { rooms: req.room._id }, balance })
                res.status(200).json({ message: `ok` })
            } else {
                res.status(400).json({ message: `It is not ur room` })
            }

        } catch (e) {
            res.status(500).json({ message: `Smth has gone wrong. ${e.message}` })
        }
    }
)







module.exports = router