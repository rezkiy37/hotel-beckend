const { Router } = require('express')
const auth = require('../middlewares/auth.middleware')
//models
const Room = require('../models/Room.model')
const Hotel = require('../models/Hotel.model')


const router = Router()


router.param('roomID', async (req, res, next, id) => {
    try {
        const room = await Room.findById(req.params.roomID).populate('guest')

        if (!room) {
            res.status(404).json({ message: 'Room is not found' })
        }

        req.room = room

        next()
    } catch (e) {
        res.status(500).json({ message: `Smth has gone wrong. ${e.message}` })
    }
})


// /api/rooms/:roomID
router.get('/:roomID', auth,
    async (req, res) => {
        try {
            res.status(200).json({ room: req.room })
        } catch (e) {
            res.status(500).json({ message: `Smth has gone wrong. ${e.message}` })
        }
    }
)

// /api/rooms/hotels/:hotelID
router.get('/hotels/:hotelID', auth,
    async (req, res) => {
        try {
            const rooms = await Room.find({ hotel: req.params.hotelID })

            if (!rooms) {
                res.status(404).json({ message: 'Rooms are not found' })
            }

            res.status(200).json(rooms)
        } catch (e) {
            res.status(500).json({ message: `Smth has gone wrong. ${e.message}` })
        }
    }
)


// /api/rooms/create
router.post('/create', auth,
    async (req, res) => {
        try {
            const { order, title, description, price, hotelID } = req.body

            const existing = await Room.findOne({ order, hotel: hotelID })


            if (existing) {
                return res.status(400).json({ message: 'This room is already exists' })
            }

            const room = new Room({
                order, title, description, price, hotel: hotelID
            })

            await room.save()

            await Hotel.findByIdAndUpdate(hotelID, { $push: { rooms: room._id } })

            res.status(201).json({ message: 'Room was created' })
        } catch (e) {
            res.status(500).json({ message: `Smth has gone wrong. ${e.message}` })
        }
    }
)

// /api/rooms/update
router.post('/:roomID', auth,
    async (req, res) => {

        const { order, title, description, price } = req.body

        const changes = {}

        if (order) changes.order = order
        if (title) changes.title = title
        if (description) changes.description = description
        if (price) changes.price = price

        const room = await Room.findByIdAndUpdate(req.params.roomID, {
            ...changes
        })

        if (!room) {
            res.status(404).json({ message: 'Rooms are not found' })
        }

        res.status(200).json({ room })
    }
)


// /api/rooms/:roomID
router.delete("/:roomID", auth,
    async (req, res) => {
        try {

            await req.room.remove()

            res.status(200).json({ room: req.room })
        } catch (e) {
            res.status(500).json({ message: `Smth has gone wrong. ${e.message}` })
        }
    }
)


module.exports = router