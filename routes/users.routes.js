const { Router } = require('express')
const config = require('config')
const auth = require('../middlewares/auth.middleware')
//models
const User = require('../models/User.model')

const router = Router()

// /api/users
router.param('userID', async (req, res, next, id) => {
    try {
        const user = await User.findById(req.params.userID)

        if (!user) {
            res.status(404).json({ message: 'User is not found' })
        }

        req.findUser = user

        next()
    } catch (e) {
        res.status(500).json({ message: `Smth has gone wrong. ${e.message}` })
    }
})

// /api/users
router.get('/', auth,
    async (req, res) => {
        try {
            const users = await User.find({})

            res.status(200).json({ users })
        } catch (error) {
            res.status(500).json({ message: `Smth has gone wrong. ${e.message}` })
        }
    }
)

// /api/users/:userID
router.get('/:userID', auth,
    async (req, res) => {
        try {
            res.status(200).json({ user: req.findUser })
        } catch (error) {
            res.status(500).json({ message: `Smth has gone wrong. ${e.message}` })
        }
    }
)

// /api/users/balance/create/:userID
router.post('/balance/create/:userID', auth,
    async (req, res) => {
        try {

            const { balance } = req.body

            const user = await User.findByIdAndUpdate(req.user.userID, { balance })

            res.status(200).json({ user })
        } catch (error) {
            res.status(500).json({ message: `Smth has gone wrong. ${e.message}` })
        }
    }
)

// /api/users/balance/topup/:userID
router.post('/balance/topup/:userID', auth,
    async (req, res) => {
        try {

            const { sum } = req.body

            const user = await User.findById(req.user.userID)

            const balance = sum + user.balance

            await User.findByIdAndUpdate(req.user.userID, { balance })

            res.status(200).json({ user })
        } catch (error) {
            res.status(500).json({ message: `Smth has gone wrong. ${e.message}` })
        }
    }
)

// /api/users/:userID
router.post('/:userID', auth,
    async (req, res) => {
        try {
            const data = {}

            const { fullname, email } = req.body

            if (fullname) data.fullname = fullname
            if (email) data.email = email

            const user = await User.findByIdAndUpdate(req.user.userID, { ...data })

            res.status(200).json({ user })
        } catch (error) {
            res.status(500).json({ message: `Smth has gone wrong. ${e.message}` })
        }
    }
)

// /api/users/:userID
router.delete('/:userID', auth,
    async (req, res) => {
        try {
            await req.findUser.remove()

            res.status(200).json({ user: req.findUser })
        } catch (error) {
            res.status(500).json({ message: `Smth has gone wrong. ${e.message}` })
        }
    }
)


module.exports = router
