const { Router } = require('express')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const config = require('config')
const { check, validationResult } = require('express-validator')
//models
const User = require('../models/User.model')


const router = Router()


// /api/auth/register
router.post(
    '/register',
    [
        check('email', 'Uncorrect email').isEmail(),
        check('password', '6 characters at least').isLength({ min: 6 }),
        check('fullname', 'Uncorrect name').exists()
    ],
    async (req, res) => {
        try {
            const error = validationResult(req)

            if (!error.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                    message: 'Uncorrect login data'
                })
            }

            const { email, fullname, password } = req.body

            const candidate = await User.findOne({ email })

            if (candidate) {
                return res.status(400).json({ message: 'This user is already exists' })
            }

            const hashedPassword = await bcrypt.hash(password, 12)

            const user = new User({ email, fullname, password: hashedPassword })

            await user.save()

            res.status(201).json({ message: 'User was created' })
        } catch (e) {
            res.status(500).json({ message: `Smth has gone wrong. ${e.message}` })
        }
    }
)

// /api/auth/login
router.post('/login',
    [
        check('email', 'Uncorrect email').normalizeEmail().isEmail(),
        check('password', 'Uncorrect password').exists()
    ],
    async (req, res) => {
        try {
            const errors = validationResult(req)

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                    message: 'Uncorrect data'
                })
            }
            const { email, password } = req.body

            const user = await User.findOne({ email })

            if (!user) {
                return res.status(400).json({ message: 'User is undefined' })
            }

            const isMatch = await bcrypt.compare(password, user.password)

            if (!isMatch) {
                return res.status(400).json({ message: 'Password is wrong' })
            }

            const token = jwt.sign(
                { userID: user._id },
                config.get('jwtSecret'),
                { expiresIn: '2h' }
            )

            res.status(200).json({ token: `Bearer ${token}`, userID: user._id })

        } catch (e) {
            console.log(e)
        }
    })




module.exports = router