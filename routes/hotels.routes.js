const { Router, json } = require('express')
const { check, validationResult } = require('express-validator')
const auth = require('../middlewares/auth.middleware')
//models
const Hotel = require('../models/Hotel.model')
const Room = require('../models/Room.model')


const router = Router()



router.param('hotelID', async (req, res, next) => {
    try {
        const hotel = await Hotel.findById(req.params.hotelID).populate('rooms')

        if (!hotel) {
            res.status(404).json({ message: 'Hotel is not found' })
        }

        req.hotel = hotel

        next()
    } catch (e) {
        res.status(500).json({ message: `Smth has gone wrong. ${e.message}` })
    }
})

// /api/hotels
router.get('/', auth,
    async (req, res) => {
        try {
            const hotels = await Hotel.find({}).populate('rooms')
            //.skip(20).limit(10)

            res.status(201).json(hotels)
        } catch (e) {
            res.status(500).json({ message: `Smth has gone wrong. ${e.message}` })
        }
    }
)

// /api/hotels/:hotelID
router.get('/:hotelID', auth,
    async (req, res) => {
        try {
            const hotel = await req.hotel.populate('rooms')

            res.status(200).json(hotel)
        } catch (e) {
            res.status(500).json({ message: `Smth has gone wrong. ${e.message}` })
        }
    }
)


// /api/hotels/create
router.post('/create', auth,
    [
        check('title', 'Uncorrect title').exists(),
        check('description', 'Uncorrect description').exists()
    ],
    async (req, res) => {
        try {
            const errors = validationResult(req)

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                    message: 'Uncorrect data'
                })
            }

            const { title, description } = req.body

            const existing = await Hotel.findOne({ title })

            if (existing) {
                res.status(401).json({ message: 'This hotel is already exists' })
            }

            const hotel = new Hotel({
                title, description
            })

            await hotel.save()

            res.status(201).json({ message: 'Hotel was created' })

        } catch (e) {
            res.status(500).json({ message: `Smth has gone wrong. ${e.message}` })
        }
    }
)

// /api/hotels/:hotelID
router.post('/:hotelID', auth,
    async (req, res) => {
        try {
            const { title, description } = req.body

            const changes = {}

            if (title) changes.title = title
            if (description) changes.description = description

            const hotel = await req.hotel.update({ ...changes })

            res.status(200).json(hotel)
        } catch (e) {
            res.status(500).json({ message: `Smth has gone wrong. ${e.message}` })
        }
    }
)

// /api/hotels/:hotelID
router.delete('/:hotelID', auth,
    async (req, res) => {
        try {
            await req.hotel.remove()

            // await Room.findOneAndRemove({ hotel: req.params.hotelID })

            res.status(200).json(req.hotel)
        } catch (e) {
            res.status(500).json({ message: `Smth has gone wrong. ${e.message}` })
        }
    }
)


module.exports = router