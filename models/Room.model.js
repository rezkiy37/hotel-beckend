const { Schema, model, Types } = require('mongoose')

const schema = new Schema({
    order: { type: Number, required: true },
    title: { type: String, required: true },
    description: { type: String, required: true },
    price: { type: Number, required: true },
    hotel: { type: Types.ObjectId, ref: 'Hotel' },
    guest: { type: Types.ObjectId, ref: 'User' }
}, {
    timestamps: true
})

schema.post('remove', async (doc) => {
    try {
        const Hotel = require('./Hotel.model')

        await Hotel.findByIdAndUpdate(doc.hotel, { $pull: { rooms: doc._id } })
    } catch (e) {
        console.log(e)
    }
})

module.exports = model('Room', schema)