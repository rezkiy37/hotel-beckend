const { Schema, model, Types } = require('mongoose')

const schema = new Schema({
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    fullname: { type: String, required: true, unique: true },
    balance: { type: Number },
    rooms: [{ type: Types.ObjectId, ref: 'Room' }]
}, {
    timestamps: true
})

schema.post('remove', async doc => {
    try {
        const Room = require('./Room.model')
        const rooms = await Room.find({ guest: doc._id })
        console.log(rooms)
        if (rooms.length) {
            rooms.forEach(async room => {
                try {
                    room.guest = undefined
                    await room.save()
                } catch (e) {
                    console.log(e)
                }
            })
        }
    } catch (e) {
        console.log(e)
    }
})

module.exports = model('User', schema)