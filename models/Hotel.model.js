const { Schema, model, Types } = require('mongoose')


const schema = new Schema({
    title: { type: String, required: true, unique: true },
    description: { type: String, required: true },
    rooms: [{ type: Types.ObjectId, ref: 'Room' }]
}, {
    timestamps: true
})


schema.post('remove', async doc => {
    try {
        const Room = require('./Room.model')
        const rooms = await Room.find({ hotel: doc._id })
        if (rooms.length) {
            rooms.forEach(async room => {
                try {
                    console.log(room)
                    await room.remove()
                } catch (e) {
                    console.log(e)
                }
            })
        }
    } catch (e) {
        console.log(e)
    }
})


module.exports = model('Hotel', schema)