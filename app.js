const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')
const config = require('config')


const app = express()

app.use(cors())

app.use(express.json({ extended: true }))

app.use('/api/auth', require('./routes/auth.routes'))
app.use('/api/rooms', require('./routes/rooms.routes'))
app.use('/api/hotels', require('./routes/hotels.routes'))
app.use('/api/rent', require('./routes/rent.routes'))
app.use('/api/users', require('./routes/users.routes'))


const PORT = config.get('PORT')
const mongoURI = config.get('mongoURI')

async function start() {
    try {
        await mongoose.connect(mongoURI, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
            useFindAndModify: false
        })
    } catch (e) {
        console.log('Server error', e.message)
    }
}

start()

app.listen(PORT, () => console.log(`Server has been starting on port ${PORT}`))